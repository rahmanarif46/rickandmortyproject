//
//  Extensions.swift
//  RickAndMortyProject
//
//  Created by Arif Rahman Sidik on 25/05/23.
//

import Foundation
import UIKit

extension UIView {
    func addSubviews(_ view: UIView...) {
        view.forEach({
            addSubview($0)
        })
    }
}
