//
//  RMEndpoint.swift
//  RickAndMortyProject
//
//  Created by Arif Rahman Sidik on 24/05/23.
//

import Foundation

/// represents unique api endpoint
@frozen enum RMEndpoint: String {
    /// end point get character info
    case character
    case location
    case episode
}
