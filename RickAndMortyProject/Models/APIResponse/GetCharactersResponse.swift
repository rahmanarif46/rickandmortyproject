//
//  GetCharactersResponse.swift
//  RickAndMortyProject
//
//  Created by Arif Rahman Sidik on 25/05/23.
//

import Foundation

struct GetCharactersResponse: Codable {
    let info: Info
    let results: [RMCharacter]
    
    struct Info: Codable {
        let count: Int
        let pages: Int
        let next: String?
        let prev: String?
    }
}
