//
//  RmFooterLoadingCollectionReusableView.swift
//  RickAndMortyProject
//
//  Created by Arif Rahman Sidik on 31/05/23.
//

import UIKit

final class RmFooterLoadingCollectionReusableView: UICollectionReusableView {
    static let identifier =  "RmFooterLoadingCollectionReusableView"
    
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.hidesWhenStopped = true
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        addSubview(spinner)
        addConstraint()
    }
    
    required init(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    func addConstraint() {
        NSLayoutConstraint.activate([
            spinner.widthAnchor.constraint(equalToConstant: 100),
            spinner.heightAnchor.constraint(equalToConstant: 100),
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
    public func startAnimating() {
        spinner.startAnimating()
    }
}
